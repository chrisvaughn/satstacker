# satstacker - self-hosted bitcoin DCA

## What it does

satstacker is a simple, self-hosted script for purchasing bitcoin at regular intervals. It has some logic for choosing how much to buy and when to buy.

## Setup

Install the Coinbase Pro python client:  
[https://github.com/danpaquin/coinbasepro-python](https://github.com/danpaquin/coinbasepro-python)

Get an API key from your Coinbase Pro account at [https://pro.coinbase.com/profile/api](https://pro.coinbase.com/profile/api):  
![push](./images/add-api-key.jpg)  

[https://help.coinbase.com/en/pro/other-topics/api/how-do-i-create-an-api-key-for-coinbase-pro](https://help.coinbase.com/en/pro/other-topics/api/how-do-i-create-an-api-key-for-coinbase-pro)

Add some funds (USD) to your Coinbase Pro portfolio from your bank account (manually).

Download satstacker.py from the repo, and call it regularly (e.g. daily) using a cron job, perhaps on an always-connected Raspberry Pi computer.  
[https://phoenixnap.com/kb/set-up-cron-job-linux](https://phoenixnap.com/kb/set-up-cron-job-linux)

### Push Notifications via IFTTT
![push](./images/push.png)  

If you want push notifications detailing what the script did each time it is run, set up an ifttt.com account, create a webhook -> push notification applet with an event name of "message" and the content set to just one wildcard, and add your trigger key to the config file as shown below ([Find your trigger key here](https://ifttt.com/maker_webhooks/settings)). Also download the [IFTTT app](https://apps.apple.com/us/app/ifttt/id660944635), sign in to your account, and enable push notifications.

In the same directory as the script, create a file called "config" and format it like this, filling in the details.  

```
{
 "api_passphrase": "",
 "api_key": "",
 "api_secret": "",
 "iffft_trigger_key": ""
}
```

If you'd prefer for the push notification to use bitcoin for units rather than sats, include another key in the config:  
"units": "BTC"

## FAQ

Why Coinbase Pro?  
It's easy and has low fees for small purchases.

What's DCA?  
Dollar cost averaging, a technique to reduce the risk of moving your wealth into an asset.  
[https://en.wikipedia.org/wiki/Dollar_cost_averaging](https://en.wikipedia.org/wiki/Dollar_cost_averaging)

What's with the name "satstacker"?  
A bitcoin is divisible into 100,000,000 units called satoshis (sats) (in honor of Satoshi Nakamoto, creator of bitcoin). A common saying among bitcoiners is that you should "stack sats", meaning save money a little at a time, adding to your "stack". Hence "satstacker".